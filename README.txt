A controller for caching large objects in unix shared memory.

This can be used for storing large constant objects, such as
the theme cache or other large drupal arrays. Although this
specific instance requires modification to drupal core, the
performance can greatly improve over other caching methods
such as memcache, when the objects are large and network
latency becomes an issue. This does use memory on your web
server that will not be released until rebooted or manually
removed with the ipcrm command, so use it sparingly on items
that are real performance issues.

This requires the php shmop extension.

Usage:

To write to shared memory:

shmop_set($name, $avlue, $size, $format)

$name: A unique name for the storage value. Calling this function
again with the same name will overwrite the previous value.
$value: The value to store. This can be an array or object.
$size: The size of memory allocation. If not provided or if NULL,
the size will be allocated at 10% larger than the current size
to allow for updated values that are slightly larger.
$form: Defaults to 'json' but can be 'serialize'. This is the
method that will be used to store the object. JSON is faster,
but might not support all PHP objects.

To retreieve from shared memory:

shmop_get($name, $format)

$name: The name that the data was stored under.
$format: The format that was used when saving the data.
